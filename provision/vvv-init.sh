#!/usr/bin/env bash
# Provision WordPress Stable

DOMAIN=`get_primary_host "${VVV_SITE_NAME}".test`
DOMAINS=`get_hosts "${DOMAIN}"`
SITE_TITLE=`get_config_value 'site_title' "${DOMAIN}"`
WP_VERSION=`get_config_value 'wp_version' 'latest'`
WP_TYPE=`get_config_value 'wp_type' "single"`
DB_NAME=`get_config_value 'db_name' "${VVV_SITE_NAME}"`
DB_USER=$(date +%s | sha256sum | base64 | head -c 32 ; echo)
DB_PASS=$(date +%s | sha256sum | base64 | head -c 64 ; echo)
DB_PREFIX=`get_config_value 'db_prefix' "$(date +%s | sha256sum | base64 | head -c 6 ; echo)_"`
THEME=`get_config_value 'theme' 'twentyseventeen'`

# Make a database, if we don't already have one
mysql -u root --password=root -e "CREATE DATABASE IF NOT EXISTS ${DB_NAME}"
mysql -u root --password=root -e "GRANT ALL PRIVILEGES ON ${DB_NAME}.* TO ${DB_USER}@localhost IDENTIFIED BY '${DB_PASS}';"

# Nginx Logs
mkdir -p ${VVV_PATH_TO_SITE}/log
touch ${VVV_PATH_TO_SITE}/log/error.log
touch ${VVV_PATH_TO_SITE}/log/access.log

# VVV-Hosts Fix
echo ${DOMAIN} > ${VVV_PATH_TO_SITE}/vvv-hosts

# Install and configure the latest stable version of WordPress
if [[ ! -f "${VVV_PATH_TO_SITE}/public_html/wp-load.php" ]]; then
  noroot wp core download --version="${WP_VERSION}"
fi

if [[ ! -f "${VVV_PATH_TO_SITE}/public_html/wp-config.php" ]]; then
  noroot wp core config --dbname="${DB_NAME}" --dbuser="${DB_USER}" --dbpass="${DB_PASS}" --dbprefix="${DB_PREFIX}" --quiet --extra-php <<PHP
define( 'WP_DEBUG', true );
PHP
fi

if ! $(noroot wp core is-installed); then

  if [ "${WP_TYPE}" = "subdomain" ]; then
    INSTALL_COMMAND="multisite-install --subdomains"
  elif [ "${WP_TYPE}" = "subdirectory" ]; then
    INSTALL_COMMAND="multisite-install"
  else
    INSTALL_COMMAND="install"
  fi

  noroot wp core ${INSTALL_COMMAND} --url="${DOMAIN}" --quiet --title="${SITE_TITLE}" --admin_name=admin --admin_email="admin@local.dev" --admin_password="password"
else
  cd ${VVV_PATH_TO_SITE}/public_html
  noroot wp core update --version="${WP_VERSION}"
fi

noroot wp option update blogdescription "${SITE_DESC}"

noroot wp option update close_comments_days_old 90

noroot wp option update close_comments_for_old_posts 1

noroot wp option update gzipcompression 1

noroot wp option update permalink_structure '/%postname%/'

noroot wp option update post_per_page 1

noroot wp option update rss_use_excerpt 1

noroot wp option update start_of_week 1

noroot wp option update use_smilies 0

if ! $(noroot wp theme is-installed twentyseventeen); then
	noroot wp theme install twentyseventeen
fi

noroot wp scaffold child-theme ${VVV_SITE_NAME} --parent_theme=twentyseventeen --theme_name=${SITE_TITLE} --author='Code Diem' --author_uri='https://www.codediem.com' --theme_uri='https://www.codediem.com' --activate

if [ $(noroot wp core check-update --format=count) > 0 ]; then
  noroot wp core update
fi

noroot wp plugin update --all
noroot wp theme update --all
noroot wp language core update

cp -f "${VVV_PATH_TO_SITE}/provision/vvv-nginx.conf.tmpl" "${VVV_PATH_TO_SITE}/provision/vvv-nginx.conf"
sed -i "s#{{DOMAINS_HERE}}#${DOMAINS}#" "${VVV_PATH_TO_SITE}/provision/vvv-nginx.conf"