# VVV Custom site template

1. [Index](#vvv-custom-site-template)
2. [Overview](#overview)
3. [Setting Up the Machine](#setting-up-the-machine)
   1. [Base Setup](#base-setup)
      1. [Ubuntu Linux](#ubuntu-linux)
      2. [Mac OSX and Microsoft Windows](#mac-osx-and-microsoft-windows)
   2. [VVV Setup](#vvv-setup)
   3. [Adding a dashboard](#adding-a-dashboard)
   4. [Our Personal Flavour](#our-personal-flavour)
4. [Handling sites](#handling-sites)
   1. [Create a new site](#create-a-new-site)
      1. [The minimum required configuration](#the-minimum-required-configuration)
      2. [Database Name](#database-name)
      3. [Hosts (Required)](#hosts-required)
      4. [Provision Repo (Required)](#provision-repo-required)
      5. [Site Title](#site-title)
      6. [Site Type](#site-type)
      7. [Skip Provisioning](#skip-provisioning)
      8. [WordPress Version](#wordpress-version)
   2. [Delete a site](#delete-a-site)
5. [Customizing this site template](#customizing-this-site-template)
6. [Troubleshooting](#troubleshooting)
7. [To Do](#to-do)

# Overview

This is a [VVV](https://varyingvagrantvagrants.org/)-based project (all thanks to their team) which uses a template based on [custom site template by Varying Vagrant Vagrants](https://github.com/Varying-Vagrant-Vagrants/custom-site-template).

Our goals are:
- Conquer the world
- Create any kind of WordPress site through presets

# Setting Up the Machine

First of all, we need to set VVV up. We've written all the process down for you because we found that the [official documentation](https://varyingvagrantvagrants.org/docs/en-US/) is... a bit complicated.

## Base setup

### Ubuntu Linux

First of all, you need your system to be fully upgraded:

```
sudo apt-get -q update && sudo apt-get -qy upgrade
```

Then, you need some extra tools:

```
sudo apt-get -qy install gdebi git virtualbox
```

Now, you need to install Vagrant. There's a bug with the version 1.8.1, so you can try to install the latest version for your system with:

```
sudo apt-get -qy install vagrant
```

Or, in case your system cannot install a newer version, you can install 1.8.0 -which works perfectly fine-:

```
wget https://releases.hashicorp.com/vagrant/1.8.0/vagrant_1.8.0_x86_64.deb
sudo gdebi -n vagrant_1.8.0_x86_64.deb
rm vagrant_1.8.0_x86_64.deb
```
### Mac OSX and Microsoft Windows

1. Go to [VirtualBox download page](https://www.virtualbox.org/wiki/Downloads) and install the latest 5.x version.

2. Go to [Vagrant download page](https://www.vagrantup.com/downloads.html) and install the latest 2.x version.

3. If you're using Windows, don't ever use that shit called PowerShell. Go to [Git SCM](https://git-scm.com/) and download a proper command line interface.

## VVV setup

Open a command line interface wherever you want to create the development environment and clone the project from GitHub:

```
git clone -b master git://github.com/Varying-Vagrant-Vagrants/VVV.git vagrant-local
cd vagrant-local
```

You need to install some Vagrant plugins in order to get Vagrant to work properly:

```
vagrant plugin install vagrant-triggers vagrant-hostsupdater
```

**VB Guest it's currently a part of the Vagrant core so we don't actually need to install it. But if you're using Vagrant previous to the 5.2 version, you'll need to install it.**

```
vagrant plugin install vagrant-vbguest
```

Almost finished! You just need to set the whole system up starting the process for the first time:

```
vagrant up
```

## Adding a dashboard

VVV allows you to look at whatever you have at the virtual machine by opening a browser tab and go into the [main VVV local website]().

Although we really like Tom's work there, we prefer to use [topdown's VVV custom dashboard](https://github.com/topdown/VVV-Dashboard).

You can install it by going to the default web's folder (your-vvv-installation-folder/www/default), deleting the dashboard folder and then:

```
git clone https://github.com/topdown/VVV-Dashboard.git dashboard
cp dashboard/dashboard-custom.php .
```

**IMPORTANT: as all current custom dashboards are developed for VVV v1, not for VVV2, you'll need a little patch in your custom site template code in order for this to work properly.**

Place this line in your custom site template code.

```
echo ${DOMAIN} > ${VVV_PATH_TO_SITE}/vvv-hosts
```

And you're done. Now you can use topdown's custom dashboard properly.

## Our Personal Flavour

Our scripts use some specific tools -jq to handle JSON files, xvfb and cutycapt to generate a theme image-, so you need to tell Vagrant to install them.

1. Open the file explorer
2. Go to your vagrant installation folder
3. Go to provision/provision.sh
  1. Look for the line where is writen ```python-pip```. I think it's in line 80
  2. Just below that line, add the following packages: jq

Finally...

1. Copy vvv-config.yml and rename it as vvv-custom.yml
2. Append this line to vvv-custom.yml

```
    - trusted-hosts
```

# Handling sites
Any new site you want to add, you need to set `vvv-custom.yml` up and our template will do the rest of the job for you. Nice, isn't it?

## Create a new site

You must set the new site up in `vvv-custom.yml`.

### The minimum required configuration

```
sites:
  my-site:
    hosts:
      - my-site.dev
    repo: git@gitlab.com:borjalofe/vvv-custom-site-template.git
```

### Database Name

You can change the default database name -your project name- using the `db_name` tag.

```
sites:
  project-name:
    custom:
      db_name: super_secret_db_name
    hosts:
      - project-name.dev
    repo: git@gitlab.com:borjalofe/vvv-custom-site-template.git
```

### Hosts (Required)

You need to see your site to check you've done your job correctly. Or in case you need to update or modify it. So you need to set one domain at least. VVV uses your `/etc/hosts` to create a false domain using 127.0.0.1 to do so.

You must to set those domains with the `hosts` tag:

```
sites:
  project-name:
    hosts:
      - project-name.dev
    repo: git@gitlab.com:borjalofe/vvv-custom-site-template.git
```

You can set as many domains as you need:

```
sites:
  project-name:
    hosts:
      - project-name.dev
      - project-name1.dev
      - project-name2.dev
      - project-name-best.dev
      - local.project-name.dev
      - site.project-name.dev
    repo: git@gitlab.com:borjalofe/vvv-custom-site-template.git
```

### Provision Repo (Required)

Our Provision repo is: [git@gitlab.com:borjalofe/vvv-custom-site-template.git]

```
sites:
  project-name:
    hosts:
      - project-name.dev
    repo: git@gitlab.com:borjalofe/vvv-custom-site-template.git
```

And we have two branches to use:

| Branch | What you get             |
|--------|--------------------------|
| master | Lastest official release |
| dev    | Lastest stable release   |

```
sites:
  project-name:
    branch: dev
    hosts:
      - project-name.dev
    repo: git@gitlab.com:borjalofe/vvv-custom-site-template.git
```

Master is the default branch so you don't need to write `branch: master` to get it.

```
sites:
  project-name:
    hosts:
      - project-name.dev
    repo: git@gitlab.com:borjalofe/vvv-custom-site-template.git
```

### Site Title

You can change the default site title -your project name- using the `site_title` tag.

```
sites:
  project-name:
    custom:
      site_title: My Awesome Dev Site
    hosts:
      - project-name.dev
    repo: git@gitlab.com:borjalofe/vvv-custom-site-template.git
```

### Site Type

Defines the WordPress environment you wish to install. The supported environments are:

- single (This is the default -optional to write- value)
- subdomain
- subdirectory

```
sites:
  project-name:
    custom:
      wp_type: subdirectory
    hosts:
      - project-name.dev
    repo: git@gitlab.com:borjalofe/vvv-custom-site-template.git
```

### Skip Provisioning

You only need to create a site once but everytime you run `vagrant up --provision`, `vagrant reload --provision` or `vagrant provision`, you're re-creating every site you've set up.

If you don't want to re-create a site, you must set `skip_provisioning` to `true`.

```
sites:
  project-name:
    hosts:
      - project-name.dev
    repo: git@gitlab.com:borjalofe/vvv-custom-site-template.git
    skip_provisioning: true
```

### WordPress Version

Defines the WordPress version you wish to install. Valid values are:

- nightly
- latest (This is the default -optional to write- value)
- a version number

```
sites:
  project-name:
    custom:
      wp_version: 4.6.4
    hosts:
      - project-name.dev
    repo: git@gitlab.com:borjalofe/vvv-custom-site-template.git
```

## Delete a site

In order to completely delete a site, you must:
1. Delete the database through:
  ```
  vagrant ssh
  mysql -uroot -proot -e "drop database if exists proyect-name;"
  exit
  rm database/backups/project-name.sql
  ```
2. Delete files:
  - In the virtual machine:
    ```
    vagrant ssh
    rm -R /srv/www/project-name
    exit
    ```
  - Or in our filesystem:
    ```
    rm -R www/project-name
    ```

# Customizing this site template

Obviously, you want to customize this *custom site template* for your own goals.

## Adding more options

VVV comes with one interesting function *get_config_value*. This method looks for info at the *custom* section of the *vvv-config.yml/vvv-custom.yml* has two args:
1. the key you want to search for
2. the default value you're going to use in case there's no info for that key

So, you can add any key to that section and access it from your custom site template thanks to *get_config_value*.

### Example 1: Adding Google Analytics ID

Almost any website you create will need Google Analytics. So why don't you setup a plugin for that task. You only need to add the Google Analytics ID to the *vvv-custom.yml* file and create de needed code into your *vvv-init.sh*.

```
sites:
  project-name:
    custom:
      google_analytics_id: UA-12345678-9
    hosts:
      - project-name.dev
    repo: git@gitlab.com:borjalofe/vvv-custom-site-template.git
```

In this example, you'll use the *google_analytics_id* key to access the info:

```
GA_ID=`get_config_value 'google_analytics_id' ''`
```

And setup your favourite Google Analytics plugin:

```
wp option add 'the_option_key_of_the_plugin' "${GA_ID}"
```

# Troubleshooting

## Error reading response length from authentication socket. Permission denied (publickey). fatal: Could not read from remote repository.

This is ssh-agent forwarding not working correctly while provisioning on Windows... at least, I didn't have problems with it when using Ubuntu Linux, nor my partner had it using Mac OSX.

The main problem is Git Bash uses a fake-unix-sockets and the ruby gem ```net-ssh``` is unaware of it. So you'll need a work-around to fix it.

1. Get [PuTTY](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html)
2. Use PuTTYgen to generate an SSH key pair (I've got problems generating a RSA key pair so I've been using DSA key pair)
3. Add the .ppk (private key) to Pageant (from the PuTTY suite)
4. Add the public key to the git repo
5. Go to the properties tab of the PuTTY shortcut and add the path to the private key in the target field
6. You're done!

For more info, this is [the main source](https://www.digitalocean.com/community/tutorials/how-to-use-pageant-to-streamline-ssh-key-authentication-with-putty).

# To Do
- Create scripts to download sites from hostings
- Create scripts to upload -deploy- sites to hostings